A Dart package that helps you work with physical quantities and units seamlessly.

Created from templates made available by Stagehand under a BSD-style [license](https://github.com/dart-lang/stagehand/blob/master/LICENSE).

## Installation

To install, add this to your `pubspec.yaml`'s `dependencies` section:

```yaml
quantities:
  git:
    url: https://codeberg.org/triallax/quantities.git
    ref: v0.2.0
```

**NOTE**: This package used to be published on pub.dev, but no releases after
v0.2.0 will be published there. Publishing to pub.dev requires a Google account,
and I really dislike Google and want to de-google my life as much as possible. I
know this means that packages published to pub.dev can't use `quantities`, and
that you can't have version bounds for this package if you install it with Git
as shown above. No, I am not sorry.

## API Status

The API is highly experimental; large changes are planned. Do **NOT** use this package in production code unless you understand and accept the consequences of this.

## Usage

```dart
import 'package:quantities/quantities.dart';

void main() {
  final height = 165(centi(meter));
  final weight = 53(kilo(gram));
  final bmi = weight / height.squared;
  print('${bmi.to(kilo(gram) / squareMeter)} kg / m^2');
}
```

## Credits

This package takes huge inspiration from [purescript-quantites](https://github.com/sharkdp/purescript-quantities), especially in its internal design and in its tests. Kudos to [sharkdp](https://github.com/sharkdp)!
