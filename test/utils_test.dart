import 'package:quantities/quantities.dart';
import 'package:test/test.dart';

void main() {
  test('convertCelsiusToKelvin works correctly', () {
    expect(convertCelsiusToKelvin(0), 273.15);
    expect(convertCelsiusToKelvin(-273.15), 0);
    expect(convertCelsiusToKelvin(37.3), 310.45);
  });

  test('convertFahrenheitToKelvin', () {
    expect(convertFahrenheitToKelvin(100), closeTo(310.928, 0.0005));
    expect(convertFahrenheitToKelvin(32), 273.15);
    expect(convertFahrenheitToKelvin(235.59), closeTo(386.256, 0.0005));
  });

  test('convertKelvinToCelsius works correctly', () {
    expect(convertKelvinToCelsius(0), -273.15);
    expect(convertKelvinToCelsius(373.15), 100);
  });

  test('convertKelvinToFahrenheit', () {
    expect(convertKelvinToFahrenheit(273.15), 32);
    expect(convertKelvinToFahrenheit(0), closeTo(-459.67, 0.0000000000001));
    expect(convertKelvinToFahrenheit(255.428), closeTo(0.1, 0.0005));
  });
}
