import 'package:quantities/quantities.dart';
import 'package:test/test.dart';

import 'utils.dart';

void main() {
  const tolerance = 0.0001;

  group('Quantity', () {
    test('constructs correct quantity', () {
      final quantity1 = Quantity(5, meter);
      expect(quantity1.value, 5);
      expect(quantity1.unit, meter);

      final quantity2 = Quantity(10, kilo(gram) * meter / second);
      expect(quantity2.value, 10);
      expect(quantity2.unit, kilo(gram) * meter / second);

      final quantity3 = Quantity(13, kilo(meter) * day / second);
      expect(quantity3.value, 1123200);
      expect(quantity3.unit, kilo(meter));
    });

    test('toString works correctly', () {
      expect(
        Quantity(10, kilo(gram) * meter / second).toString(),
        '10 kg · m / s',
      );

      expect(3().toString(), '3');
      expect(6.6(pound).toString(), '6.6 lb');
      expect(3.3(kelvin).toString(), '3.3 K');
      expect(4(nano(meter)).toString(), '4 nm');
    });

    test('== works correctly', () {
      expect(3(meter), isNot(3(second)));
      expect(3(meter), isNot(3(squareMeter)));
      expect(3(meter), isNot(3.01(meter)));
      expect(90(centi(meter)), isNot(0.9(meter)));
      expect(Quantity(1), Quantity(1));
      expect(Quantity(50, kilo(meter)), 50(kilo(meter)));
      expect(10(inch), 10(inch));

      expect(Quantity(30, day), isNot(Quantity(25, day)));
      expect(Quantity(50, meter), isNot(Quantity(30, gram)));
    });

    test('hashCode works correctly', () {
      expect(50(kilo(gram)).hashCode, 50(kilo(gram)).hashCode);
      expect(1000(meter).hashCode, Quantity(1000, meter).hashCode);
      expect(
        (1000(kilo(meter)) / 0.7(day)).hashCode,
        (1000(kilo(meter)) / 0.7(day)).hashCode,
      );
    });

    test('negation works correctly', () {
      expect(-30(meter), (-30)(meter));
      expect(-5(meter / second), (-5)(meter / second));
      expect(-0(second), 0(second));
    });

    test('subtraction works correctly', () {
      expect(35(meter) - 2(meter), 33(meter));
      expect(5(kilo(meter)) - 300(meter), 4.7(kilo(meter)));
      expect(5() - 300(), -295());
      expect(1(hour) - 15(minute), 0.75(hour));
      expect(() => 3(second) - 5(meter), throwsArgumentError);
    });

    test('addition works correctly', () {
      expect(3(meter) + 5(meter), 8(meter));
      expect(500(second) + 3(day), 259700(second));
      expectQuantity(
        5(meter / second) + 1000(kilo(meter) / day),
        16.5741(meter / second),
        tolerance,
      );
      expect((5(inch) + -6(meter)).to(inch), closeTo(-231.22, 0.001));
      expect(30(minute) + 50.4(second), 30.84(minute));
      expect(() => 3(meter) + 5(second), throwsArgumentError);
    });

    test('multiplication works correctly', () {
      final quantity1 = 50(meter) * 100(meter);

      expect(quantity1, 5000(squareMeter));
      expect(quantity1, 1000(meter) * 5(meter));

      expectQuantity(
        (50(kilo(meter)) / 0.1(day)) * 270(second),
        1.5625(kilo(meter)),
        tolerance,
      );

      expectQuantity(5(percent) + 0.03(), 8(percent), 0);

      expect((4(pascal) * 0.5(squareMeter)).to(newton), 2);
    });

    test('division works correctly', () {
      expect(45(meter) / 6(second), 7.5(meter / second));

      expect(0.3(bar) / 2(second) * 4.6(second), 0.69(bar));

      final quantity2 = 50(kilo(gram)) / 165(squareMeter);

      expect(quantity2.value, 50 / 165);
      expect(quantity2.unit, kilo(gram) / squareMeter);

      expect(
        (30(newton / (inch * inch)) / 15(pascal)).to(Unit.unity),
        closeTo(3100.01, 0.01),
      );

      final quantity3 = 5000(nano(meter)) / 1(second);

      expect(quantity3.to(meter / second), 5000 / 1000000000);
      expect(
        quantity3.to(micro(meter) / second),
        closeTo(5, 0.000000000000001),
      );

      expect(5(meter) / 4(meter) / 10(meter), 0.125(meter.reciprocal));
      expect(1() / 4(meter), 0.25(meter.reciprocal));

      expect(1() / 2(second), 0.5(second.reciprocal));
      expect(1() / 2(kilo(second)), 0.5(kilo(second).reciprocal));
    });

    test('abs works correctly', () {
      expect(2.4(second).abs(), 2.4(second));
      expect((-2.4(second)).abs(), 2.4(second));
      expect(0(second).abs(), 0(second));
    });

    test('converts between units correctly', () {
      expect(4.4().to(Unit.unity), 4.4);
      expect(90(pascal).to(pascal), 90);
      expect(90(centi(meter)).to(meter), 0.9);
      expect(5.3(pound).to(kilo(gram)), closeTo(2.40404, 0.000001));
      expect(3(week).to(day), 21);
      expect(1(meter).to(centi(meter)), 100);
      expect(500(centi(meter)).to(meter), 5);
      expect(25(day).to(month), 0.8213721020965523);
      expect(12(month).to(year), 1);
      expect(2(year).to(month), 24);
      expect(() => 1(meter).to(second), throwsArgumentError);
      expect(351(kilo(meter) / day).to(meter / second), 4.0625);
      expect(6(hour).to(day), 0.25);
      expect(
        (4(kilo(gram)) * 55(meter) / (44(second) * 3(second)))
            .to(gram * kilo(meter) / (day * day)),
        closeTo(12441600000, tolerance),
      );
      expect(5(liter).to(meter * meter * meter), 0.005);
      expect(
        (2.4(milli(mole)) / 5(deci(liter))).to(centi(mole) / milli(liter)),
        closeTo(0.00048, 0.0000000000000000001),
      );
      expect(2(mmHg).to(cmH2O), 2.71902);
      expect(1(minute).to(second), 60);
      expect(2(hour).to(minute), 120);
      expect(36(kilo(meter) / hour).to(meter / second), 10);
      expect(1(newton).to(kilo(gram) * meter / (second * second)), 1);

      expect((5(milli(ampere)) * 3(second)).to(milli(coulomb)), 15);
      expect(120(second).to(minute), 2);
      expect(1(squareMeter).to(inch * inch), closeTo(1550.0031, 1e-8));

      expect(100(inch).to(meter), 2.54);
      expect(2.54(meter).to(inch), 100);
      expect(2(kilo(meter)).to(meter), 2000);
      expect(2000(meter).to(kilo(meter)), 2);
      expect(() => 2(meter).to(squareMeter), throwsArgumentError);
    });

    test('toCelsius works correctly', () {
      expect(273.15(kelvin).toCelsius(), 0);
    });

    test('toFahrenheit works correctly', () {
      expect(273.15(kelvin).toFahrenheit(), 32);
    });

    test('squared works correctly', () {
      expect(5(meter).squared, 25(squareMeter));
    });
  });

  group('NumToQuantity', () {
    test('call is same as default Quantity constructor', () {
      expect(5(meter / second), Quantity(5, meter / second));
      expect(3(meter), Quantity(3, meter));
      // ignore: avoid_redundant_argument_values
      expect(3(Unit.unity), Quantity(3));
      expect(4(), Quantity(4));
    });
  });
}
