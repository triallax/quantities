import 'package:quantities/quantities.dart';
import 'package:quantities/src/base_unit.dart';
import 'package:quantities/src/simplified_unit.dart';
import 'package:quantities/src/standard_units.dart';
import 'package:quantities/src/unit.dart';
import 'package:quantities/src/units.dart';
import 'package:test/test.dart';

import 'utils.dart';

void main() {
  group('Unit', () {
    test('parses units from strings correctly', () {
      expect(Unit.tryParse('1'), Unit.unity);
      expect(Unit.tryParse('kN/m'), kilo(newton) / meter);
      expect(Unit.tryParse('atm/min'), standardAtmosphere / minute);
      expect(Unit.tryParse('kg*m/s/s'), kilo(gram) * meter / (second * second));
      expect(Unit.tryParse('kg'), kilo(gram));
      expect(Unit.tryParse('cm'), centi(meter));
      expect(Unit.tryParse('kg/m/m'), kilo(gram) / (meter * meter));
      expect(Unit.tryParse('kg/dL'), kilo(gram) / deci(liter));
      expect(Unit.tryParse('vg'), null);
      expect(Unit.tryParse('mmol/dL'), milli(mole) / deci(liter));
      expect(Unit.tryParse('foo'), null);
      expect(Unit.tryParse('*'), null);
      expect(Unit.tryParse('kg*'), null);
      expect(Unit.tryParse('*/1'), null);
      expect(Unit.tryParse('Nk'), null);
      expect(Unit.tryParse(''), null);
      expect(Unit.tryParse('amt'), null);
      expect(Unit.tryParse('%*m'), percent * meter);

      for (final unit in allUnits) {
        expect(Unit.tryParse(unit.toString()), unit);
      }

      for (final prefix in UnitPrefix.values) {
        expect(
          Unit.tryParse('${prefix}m/hr'),
          meter.withPrefix(prefix) / hour,
        );

        expect(
          Unit.tryParse('${prefix}g*${prefix}s'),
          gram.withPrefix(prefix) * second.withPrefix(prefix),
        );

        for (final unit in allUnits) {
          // "min" is "minute," not "milli inch."
          if (unit == inch && prefix == milli) continue;

          expect(Unit.tryParse('$prefix$unit'), unit.withPrefix(prefix));
        }
      }
    });

    test('toString returns correct string', () {
      expect(Unit.unity.toString(), '1');

      expect(second.toString(), 's');
      expect(gram.toString(), 'g');
      expect(meter.toString(), 'm');
      expect(week.toString(), 'wk');
      expect(nano(gram).toString(), 'ng');
      expect(squareMeter.toString(), 'm²');

      expect(kilo(gram).toString(), 'kg');
      expect(centi(meter).toString(), 'cm');

      var unit = Unit.unity;

      expect((unit *= centi(meter)).toString(), 'cm');
      expect((unit *= centi(meter)).toString(), 'cm²');
      expect((unit *= centi(meter)).toString(), 'cm³');
      expect((unit *= centi(meter)).toString(), 'cm⁴');
      expect((unit *= centi(meter)).toString(), 'cm⁵');
      expect((unit *= centi(meter)).toString(), 'cm⁶');
      expect((unit *= centi(meter)).toString(), 'cm⁷');
      expect((unit *= centi(meter)).toString(), 'cm⁸');
      expect((unit *= centi(meter)).toString(), 'cm⁹');
      expect((unit *= centi(meter)).toString(), 'cm¹⁰');

      expect(month.toString(), 'mo');
      expect(year.reciprocal.toString(), '1 / yr');
      expect((meter * meter).reciprocal.toString(), '1 / m²');

      expect((kilo(meter) / day).toString(), 'km / d');
      expect((inch * pound).toString(), 'in · lb');
      expect((kilo(meter) / second / second).toString(), 'km / s²');
      expect((kilo(gram) * meter / second / second).toString(), 'kg · m / s²');
      expect(
        (meter * meter * meter / (second * second * gram)).toString(),
        'm³ / (s² · g)',
      );
      expect((meter * kilo(second) * second).toString(), 'm · ks · s');

      for (final prefix in UnitPrefix.values) {
        final meterWithPrefix = meter.withPrefix(prefix);
        expect(meterWithPrefix.toString(), '${prefix}m');
        expect((meterWithPrefix * meterWithPrefix).toString(), '${prefix}m²');
      }
    });

    test('multiplies units without regards to order', () {
      expect(meter * kilo(meter) * second, kilo(meter) * second * meter);
    });

    test('multiplies two non derived units correctly', () {
      expect(kelvin * Unit.unity, kelvin);
      expect(Unit.unity * standardAtmosphere, standardAtmosphere);

      checkDerivedUnit(
        kilo(gram) * meter,
        unitsUp: [gram.baseUnit.withPrefix(kilo), meter.baseUnit],
        unitsDown: const [],
      );
    });

    test('multiplies a derived unit and non derived unit correctly', () {
      checkDerivedUnit(
        (kilo(meter) / day) * second,
        unitsUp: [
          meter.baseUnit.withPrefix(kilo),
          second.baseUnit,
        ],
        unitsDown: [day.baseUnit],
      );
    });

    test('multiplies a non derived unit and derived unit correctly', () {
      checkDerivedUnit(
        kilo(meter) * (kilo(gram) / meter),
        unitsUp: [
          meter.baseUnit.withPrefix(kilo),
          gram.baseUnit.withPrefix(kilo),
        ],
        unitsDown: [
          meter.baseUnit,
        ],
      );
    });

    test('multiplies derived units correctly', () {
      checkDerivedUnit(
        (kilo(meter) / second) * (kilo(gram) * meter),
        unitsUp: [
          meter.baseUnit.withPrefix(kilo),
          gram.baseUnit.withPrefix(kilo),
          meter.baseUnit,
        ],
        unitsDown: [second.baseUnit],
      );
    });

    test('divides two non derived units correctly', () {
      expect(meter / meter, Unit.unity);
      expect((meter * meter) / meter, meter);

      checkDerivedUnit(
        meter / second,
        unitsUp: [meter.baseUnit],
        unitsDown: [second.baseUnit],
      );

      checkDerivedUnit(
        kilo(gram) / deci(liter),
        unitsUp: [gram.baseUnit.withPrefix(kilo)],
        unitsDown: [liter.baseUnit.withPrefix(deci)],
      );

      expect(second / Unit.unity, second);

      checkDerivedUnit(
        second / second / second,
        unitsUp: const [],
        unitsDown: [second.baseUnit],
      );
    });

    test('divides a derived unit and a non derived unit correctly', () {
      checkDerivedUnit(
        (kilo(meter) / second) / meter,
        unitsUp: [meter.baseUnit.withPrefix(kilo)],
        unitsDown: [
          second.baseUnit,
          meter.baseUnit,
        ],
      );
    });

    test('divides a non derived unit and derived unit correctly', () {
      checkDerivedUnit(
        kilo(gram) / (meter * meter),
        unitsUp: [gram.baseUnit.withPrefix(kilo)],
        unitsDown: [
          meter.baseUnit,
          meter.baseUnit,
        ],
      );

      checkDerivedUnit(
        nano(meter) / micro(meter),
        unitsUp: [meter.baseUnit.withPrefix(nano)],
        unitsDown: [meter.baseUnit.withPrefix(micro)],
      );
    });

    test('divides derived units correctly', () {
      checkDerivedUnit(
        (kilo(gram) * meter) / (second / gram),
        unitsUp: [
          gram.baseUnit.withPrefix(kilo),
          meter.baseUnit,
          gram.baseUnit,
        ],
        unitsDown: [second.baseUnit],
      );

      // TODO: improve the simplification algorithm so it can handle things like (kN / m^2) / Pa better.
    });

    test('gets correct reciprocal', () {
      checkDerivedUnit(
        gram.reciprocal,
        unitsUp: const [],
        unitsDown: [gram.baseUnit],
      );

      checkDerivedUnit(
        (kilo(meter) / day).reciprocal,
        unitsUp: [day.baseUnit],
        unitsDown: [meter.baseUnit.withPrefix(kilo)],
      );
    });

    test('constructs unit without duplicate tuple in unitsUp and unitsDown',
        () {
      checkNonDerivedUnit(
        kilo(meter) * second / second,
        meter.baseUnit.withPrefix(kilo),
      );
    });

    test('simplify works correctly', () {
      expect(meter.simplify(), SimplifiedUnit(meter, 1));
      expect(
        (second / second / second).simplify(),
        SimplifiedUnit(Unit.unity / second, 1),
      );
      expect((meter / second).simplify(), SimplifiedUnit(meter / second, 1));
      expect(
        (milli(meter) / meter).simplify(),
        const SimplifiedUnit(Unit.unity, 1 / 1000),
      );

      final simplifiedUnit = (meter / second * day).simplify();
      checkNonDerivedUnit(simplifiedUnit.unit, meter.baseUnit);
      expect(simplifiedUnit.factor, 86400);
    });

    test('== works correctly', () {
      expect(Unit.unity, Unit.unity);
      expect(Unit.unity, second / second);
      expect(Unit.unity, isNot(second.reciprocal));

      expect(meter / second, meter / second);
      expect(meter / second, isNot(second / meter));

      expect(meter, isNot(second));

      expect(kilo(gram) * meter / second, isNot(kilo(gram) * meter));
      expect(kilo(meter), isNot(meter));
      expect(kilo(meter), isNot(nano(meter)));
    });

    test('hashCode works correctly', () {
      expect(Unit.unity.hashCode, (second / second).hashCode);
      expect((meter / second).hashCode, (meter / second).hashCode);
      expect(kilo(gram).hashCode, kilo(gram).hashCode);
    });

    test('toStandardUnits works correctly', () {
      final standardUnits = minute.toStandardUnits();
      expect(standardUnits.unitsUp, [secondBaseUnit]);
      expect(standardUnits.unitsDown, isEmpty);
      expect(standardUnits.factor, 60);
    });
  });
}
