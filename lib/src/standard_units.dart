import 'package:collection/collection.dart';
import 'package:meta/meta.dart';

import 'base_unit.dart';

class _SameQuantityEquality implements Equality<BaseUnit> {
  const _SameQuantityEquality();

  @override
  bool equals(BaseUnit unit1, BaseUnit unit2) => unit1.hasSameQuantity(unit2);

  @override
  int hash(BaseUnit unit) => unit.hashCode;

  @override
  bool isValidKey(Object? obj) => obj is BaseUnit;
}

@internal
class StandardUnits {
  const StandardUnits({
    required this.unitsUp,
    required this.unitsDown,
    required this.factor,
  });

  final List<BaseStandardUnit> unitsUp;

  final List<BaseStandardUnit> unitsDown;

  final double factor;

  StandardUnits simplify() {
    final newUnitsUp = unitsUp.toList();
    final newUnitsDown = unitsDown.toList();

    for (final unit in unitsUp) {
      if (newUnitsDown.remove(unit)) {
        newUnitsUp.remove(unit);
      }
    }

    return StandardUnits(
      unitsUp: newUnitsUp,
      unitsDown: newUnitsDown,
      factor: factor,
    );
  }

  bool canBeConvertedTo(StandardUnits other) {
    const equality = UnorderedIterableEquality(_SameQuantityEquality());

    // ignore: parameter_assignments
    other = other.simplify();
    final self = simplify();

    return equality.equals(self.unitsUp, other.unitsUp) &&
        equality.equals(self.unitsDown, other.unitsDown);
  }
}
