import 'package:meta/meta.dart';

import 'unit.dart';
import 'units.dart';
import 'utils.dart';

@immutable
class Quantity implements Comparable<Quantity> {
  factory Quantity(double value, [Unit unit = Unit.unity]) {
    if (unit == Unit.unity) {
      return Quantity._(value, unit);
    }

    final simplifiedUnit = unit.simplify();
    return Quantity._(value * simplifiedUnit.factor, simplifiedUnit.unit);
  }

  const Quantity._(this.value, this.unit);

  final double value;

  final Unit unit;

  Quantity operator +(Quantity that) => Quantity._(value + that.to(unit), unit);

  Quantity operator -(Quantity that) => Quantity._(value - that.to(unit), unit);

  Quantity operator -() => Quantity._(-value, unit);

  Quantity operator *(Quantity that) {
    final simplifiedUnit = (unit * that.unit).simplify();
    final newValue = value * that.value * simplifiedUnit.factor;

    return Quantity._(newValue, simplifiedUnit.unit);
  }

  Quantity operator /(Quantity that) {
    final simplifiedUnit = (unit / that.unit).simplify();
    final newValue = (value / that.value) * simplifiedUnit.factor;

    return Quantity._(newValue, simplifiedUnit.unit);
  }

  Quantity abs() {
    if (value.isNegative) {
      return -this;
    }
    return this;
  }

  double to(Unit newUnit) {
    if (unit == newUnit) return value;

    final oldStandardUnits = unit.toStandardUnits();
    final newStandardUnits = newUnit.toStandardUnits();

    if (!oldStandardUnits.canBeConvertedTo(newStandardUnits)) {
      throw ArgumentError("Can't convert $this to $newUnit");
    }

    // Read `isFinite`'s docs.
    if (!value.isFinite) {
      return value;
    }

    return value * oldStandardUnits.factor / newStandardUnits.factor;
  }

  double toCelsius() => convertKelvinToCelsius(to(kelvin));

  double toFahrenheit() => convertKelvinToFahrenheit(to(kelvin));

  bool operator <(Quantity that) => value < that.to(unit);

  bool operator <=(Quantity that) => value <= that.to(unit);

  bool operator >(Quantity that) => value > that.to(unit);

  bool operator >=(Quantity that) => value >= that.to(unit);

  @override
  int compareTo(Quantity quantity) => value.compareTo(quantity.to(unit));

  @override
  bool operator ==(Object that) =>
      identical(this, that) ||
      (that is Quantity && value == that.value && unit == that.unit);

  @override
  int get hashCode => Object.hash(value, unit);

  @override
  String toString() {
    var valueString = value.toString();

    if (valueString.endsWith('.0')) {
      valueString = valueString.substring(0, valueString.length - 2);
    }

    if (unit == Unit.unity) return valueString;

    return '$valueString $unit';
  }

  Duration toDuration() =>
      Duration(microseconds: (to(second) * 1000000).truncate());

  Quantity get squared => this * this;
}

extension NumToQuantity on num {
  Quantity call([Unit unit = Unit.unity]) => Quantity(toDouble(), unit);
}
