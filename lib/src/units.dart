import 'package:meta/meta.dart';

import 'base_unit.dart';
import 'prefixed_base_unit.dart';
import 'unit.dart';

NonDerivedUnit _mkStandard(BaseStandardUnit baseUnit) =>
    NonDerivedUnit(PrefixedBaseUnit(baseUnit));

NonDerivedUnit _mkNonStandard(
  String symbol,
  double factor,
  List<BaseStandardUnit> unitsUp, [
  List<BaseStandardUnit> unitsDown = const [],
]) =>
    NonDerivedUnit(
      PrefixedBaseUnit(BaseUnit(symbol, factor, unitsUp, unitsDown)),
    );

/// Base unit of electric current.
final ampere = _mkStandard(ampereBaseUnit);

/// Unit of electric charge, `1 C = 1 A * s`.
final coulomb = _mkNonStandard('C', 1, const [ampereBaseUnit, secondBaseUnit]);

/// Base unit of length.
final meter = _mkStandard(meterBaseUnit);

/// Unit of length, `1 in. = 0.0254 m`.
final inch = _mkNonStandard('in', 0.0254, const [meterBaseUnit]);

/// Unit of area.
final squareMeter = meter * meter;

/// Unit of volume, `1 L = 0.001 m^3`.
final liter = _mkNonStandard(
  'L',
  0.001,
  const [meterBaseUnit, meterBaseUnit, meterBaseUnit],
);

/// Unit of force, `1 N = 1 kg * m / s^2`.
final newton = _mkNonStandard(
  'N',
  1000,
  const [gramBaseUnit, meterBaseUnit],
  const [secondBaseUnit, secondBaseUnit],
);

/// Unit of pressure, `1 Pa = 1 N / m^2`.
final pascal = _mkNonStandard(
  'Pa',
  1000,
  const [gramBaseUnit],
  const [secondBaseUnit, secondBaseUnit, meterBaseUnit],
);

/// Unit of pressure, `1 bar = 100,000 Pa`.
final bar = _mkNonStandard(
  'bar',
  1000 * 100 * 1000,
  const [gramBaseUnit],
  const [secondBaseUnit, secondBaseUnit, meterBaseUnit],
);

/// Unit of pressure, `1 cmH₂O = 98.0665 Pa`.
final cmH2O = _mkNonStandard(
  'cmH₂O',
  1000 * 98.0665,
  const [gramBaseUnit],
  const [secondBaseUnit, secondBaseUnit, meterBaseUnit],
);

/// Unit of pressure, `1 mmHg = 133.322387415 Pa`.
final mmHg = _mkNonStandard(
  'mmHg',
  1000 * 133.322387415,
  const [gramBaseUnit],
  const [secondBaseUnit, secondBaseUnit, meterBaseUnit],
);

/// Unit of pressure, `1 atm = 101,325 Pa`.
final standardAtmosphere = _mkNonStandard(
  'atm',
  1000 * 101325,
  const [gramBaseUnit],
  const [secondBaseUnit, secondBaseUnit, meterBaseUnit],
);

/// Base unit of mass (note that the kilogram is the standard unit of mass in
/// SI, *NOT* the gram).
final gram = _mkStandard(gramBaseUnit);

/// Unit of mass, `1 lb = 453.59237 g`.
final pound = _mkNonStandard('lb', 453.59237, const [gramBaseUnit]);

/// Base unit for amount of substance.
final mole = _mkStandard(moleBaseUnit);

/// Base unit of time.
final second = _mkStandard(secondBaseUnit);

const _secondsPerMinute = 60.0;

/// Unit of time, `1 min = 60 s`.
final minute = _mkNonStandard('min', _secondsPerMinute, const [secondBaseUnit]);

const _secondsPerHour = _secondsPerMinute * 60;

/// Unit of time, `1 hr = 3,600 s`.
final hour = _mkNonStandard('hr', _secondsPerHour, const [secondBaseUnit]);

const _secondsPerDay = _secondsPerHour * 24;

/// Unit of time, `1 d = 24 h`.
final day = _mkNonStandard('d', _secondsPerDay, const [secondBaseUnit]);

const _secondsPerWeek = _secondsPerDay * 7;

/// Unit of time, `1 wk = 7 d`.
final week = _mkNonStandard('wk', _secondsPerWeek, const [secondBaseUnit]);

const _secondsPerMonth = _secondsPerDay * 30.436875;

/// Unit of time, `1 mo = 30.436875 d`.
final month = _mkNonStandard('mo', _secondsPerMonth, const [secondBaseUnit]);

const _secondsPerYear = _secondsPerMonth * 12;

/// Unit of time, `1 yr = 12 mo`.
final year = _mkNonStandard('yr', _secondsPerYear, const [secondBaseUnit]);

/// Base unit of temperature.
final kelvin = _mkStandard(kelvinBaseUnit);

/// Dimensionless unit, `1% = 1/100`.
final percent = _mkNonStandard('%', 1 / 100, const []);

@internal
final allUnits = [
  ampere,
  coulomb,
  meter,
  inch,
  liter,
  newton,
  pascal,
  bar,
  mmHg,
  cmH2O,
  standardAtmosphere,
  mole,
  gram,
  pound,
  second,
  minute,
  hour,
  day,
  week,
  month,
  year,
  kelvin,
  percent,
];
