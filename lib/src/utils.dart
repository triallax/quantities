const _kelvinCelsiusOffset = 273.15;

double convertCelsiusToKelvin(double celsius) => celsius + _kelvinCelsiusOffset;

double convertFahrenheitToKelvin(double fahrenheit) =>
    5 * (fahrenheit - 32) / 9 + _kelvinCelsiusOffset;

double convertKelvinToFahrenheit(double kelvin) =>
    9 * (kelvin - _kelvinCelsiusOffset) / 5 + 32;

double convertKelvinToCelsius(double kelvin) => kelvin - _kelvinCelsiusOffset;
