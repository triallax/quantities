import 'package:meta/meta.dart';

import 'unit.dart';

@internal
class SimplifiedUnit {
  const SimplifiedUnit(this.unit, this.factor);

  final Unit unit;

  final double factor;

  @override
  int get hashCode => Object.hash(unit, factor);

  @override
  bool operator ==(Object that) =>
      identical(this, that) ||
      (that is SimplifiedUnit && unit == that.unit && factor == that.factor);
}
