## 0.2.0

**IMPORTANT**: this is the last version that will be published to `pub.dev`; see
https://codeberg.org/triallax/quantities/src/commit/8aad1c9ce69aabc0526b6029e06d4acffdab83c4#installation
for details and how to receive future versions of `quantities`.

- [BREAKING] Remove old prefix syntax; to migrate, change e.g. `kilo.gram` to
  `kilo(gram)`, and `centi.meter` to `centi(meter)`
- [BREAKING] Raise Dart version lower bound to 2.14.0
- [TECHNICALLY BREAKING] Don't consider `runtimeType` in `Quantity`'s `==` and
  `hashCode`; for almost all code this change will break nothing
- Add new units:
  - Current: ampere
  - Electric charge: coulomb
  - Pressure: bar
- Remove `tuple` dependency
- Improve the documentation a bit

## 0.1.2

- Add new units:
  - Force: newton
  - Pressure: pascal, standard atmosphere, millimeter of mercury, centimeter of water
  - Time: minute
  - Temperature: kelvin, degree Celsius, degree Fahrenheit (note that the latter two have not been added as units per se because they require additive offsets)
  - Dimensionless: percent
- Add nano prefix

## 0.1.1

- Add `purescript-quantities` license to LICENSE
- Update copyright range in LICENSE

## 0.1.0

- [BREAKING] `Quantity`'s constructor now takes `double` instead of `num`
- [BREAKING] Removed `Unit.identity`; use `Unit.unity` instead
- [DEPRECATION] Deprecated old prefix syntax (i.e. `kilo.gram`); use
  new syntax (`kilo(gram)`) instead
- Migrate to null safety
- Add hour, liter, and mole units
- Add milli, deci, and micro prefixes

## 0.0.2

- Add inch and pound units

## 0.0.1

- Initial version
